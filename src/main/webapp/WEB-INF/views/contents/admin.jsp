<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="px-content">

	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li>운영자 관리</li>
		<li class="active">운영자 리스트</li>
	</ol>

	<div class="page-header">
		<h1>운영자 리스트</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-outline btn-3d">신규등록</button>
					<button type="button" class="btn btn-info btn-outline btn-3d">정보수정</button>
					<button type="button" class="btn btn-danger btn-outline btn-3d">삭제</button>
					<button type="button" class="btn btn-outline btn-3d">엑셀
						다운로드</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel box">
				<div class="box-row">
					<table id="table" data-toggle="table" data-show-refresh="true"
						data-search="true" data-pagination="true"
						data-url="./admin/getlist.do">
						<thead>
							<tr>
								<th data-field="" data-checkbox="true"></th>
								<th data-field="idx" data-sortable="true">번호</th>
								<th data-field="username" data-sortable="true">아이디</th>
								<th data-field="authority" data-sortable="true">권한</th>
								<th data-field="groupname" data-sortable="true">그룹</th>
								<th data-field="nickname" data-sortable="true">이름</th>
								<th data-field="moddate" data-sortable="true">업데이트 일자</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
