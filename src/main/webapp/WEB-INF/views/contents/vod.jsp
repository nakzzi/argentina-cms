<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="px-content">

	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li class="active">셋탑 관리</li>
	</ol>

	<div class="page-header">
		<h1>셋탑 리스트</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-outline btn-3d">신규등록</button>
					<button type="button" class="btn btn-info btn-outline btn-3d">정보수정</button>
					<button type="button" class="btn btn-danger btn-outline btn-3d">삭제</button>
					<button type="button" class="btn btn-outline btn-3d">엑셀
						다운로드</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel box">
				<div class="box-row">
					<table id="table" data-toggle="table" data-show-refresh="true"
						data-search="true" data-pagination="true"
						data-url="./vod/getlist.do">
						<thead>
							<tr>
								<th data-field="" data-checkbox="true"></th>
								<th data-field="idx" data-sortable="true">번호</th>
								<th data-field="type" data-sortable="true">종류</th>
								<th data-field="root_category" data-sortable="true">대메뉴</th>
								<th data-field="child_category" data-sortable="true">소메뉴</th>
								<th data-field="title" data-sortable="true">이름</th>
								<th data-field="moddate" data-sortable="true">업데이트 날짜</th>
								<th data-field="status" data-sortable="true">상태</th>
								<th data-field="desc" data-sortable="true">특수문장</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>