<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="px-content">
	<center><h1>test asshole</h1></center>
	
	<c:url var="logoutUrl" value="/signout" />
	<form action="${logoutUrl}" method="post">
		<input type="submit" value="로그아웃 씨발년아" /> 
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	
	<button onclick="testajax()" >test button</button>
	
	<c:url var="actionUrl" value="upload?${_csrf.parameterName}=${_csrf.token}"/>
	<form id="fileuploadForm" action="${actionUrl}" method="POST" enctype="multipart/form-data">
		<div class="header">
	  		<h2>Form</h2>
	  		<c:if test="${not empty message}">
				<div id="message" class="success">${message}</div>	  		
	  		</c:if>
		</div>
		<label for="file">File</label>
		<input id="file" type="file" name="file" />
		<p><button type="submit">Upload</button></p>		
	</form>
	
	<div class="panel">
		<div class="panel-heading">
			<div class="panel-title">view "test" table</div>
		</div>
		<div class="panel-body">
			<table id="table" data-toggle="table" data-show-refresh="true"
				data-search="true" data-pagination="true"
				data-url="./test/getlist.do">
				<thead>
					<tr>
						<th data-field="" data-checkbox="true"></th>
						<th data-field="idx" data-sortable="true">번호</th>
						<th data-field="testvarchar" data-sortable="true">varchar</th>
						<th data-field="testint" data-sortable="true">int</th>
						<th data-field="testtext" data-sortable="true">text</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	
	<h3>insert</h3>
	<form id="form1" method="post">
		varchar : <input type="text" name="testvarchar" required><br> 
		int : <input type="number" name="testint" value="1"><br>
		text : <input type="text" name="testtext" required><br>
		<p><button type="submit">Submit</button></p>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	
	<h3>update</h3>
	<form id="form2" method="post">
		Index : <input type="number" name="idx" id="idx" value="1"><br>
		varchar : <input type="text" name="testvarchar" required><br> 
		int : <input type="number" name="testint" value="1"><br>
		text : <input type="text" name="testtext" required><br>
		<p><button type="submit">Submit</button></p>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	
	<h3>delete</h3>
	<form id="form3" method="post">
		Index : <input type="number" name="idx" value="1"><br> 
		<p><button type="submit">Submit</button></p>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
	
	<h3>getBean</h3>
	<form id="form4" method="post">
		Index : <input type="number" name="idx" id="idx"><button type="submit">idx에 해당하는 bean 가져오기</button><br>
		varchar : <input type="text" name="testvarchar"><br> 
		int : <input type="number" name="testint"><br>
		text : <input type="text" name="testtext"><br>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	</form>
</div>

<!-- Your scripts -->
<script src="js/jquery.formautofill.min.js"></script>
<script>

	$('#table').on('click', 'tr', function(event) {
		  if($(this).hasClass('selected')){
		    $(this).removeClass('selected'); 
		  } else {
		    $(this).addClass('selected').siblings().removeClass('selected');
		  }
	});
	
	function testajax() {
		var token = $("meta[name='_csrf']").attr("content");
		var header = $("meta[name='_csrf_header']").attr("content");

		$.ajax({
			type : "POST",
			beforeSend : function(request) {
				request.setRequestHeader(header, token);
			},
			url : "./test/getlist.do",
			success : function(data) {
				console.log(data);
			}
		});
	}


	$(function() {
		$("#form1").submit(
				function() {
					$.post('./test/insert.do', $(this).serialize(),
							function(status) {
								// status code 2xx (success)
								if (status.code / 100 == 2) {
									//alert('굿잡~~! 성공^^*');
									$('#form1')[0].reset();
								} else { // fail
									alert(status.message);
								}
							});
					return true;
				});

		$("#form2").submit(
				function() {
					$.post('./test/update.do', $(this).serialize(),
							function(status) {
								// status code 2xx (success)
								if (status.code / 100 == 2) {
									//alert('굿잡~~! 성공^^*');
									$('#form2')[0].reset();
								} else { // fail
									alert(status.message);
								}
							});
					return true;
				});

		$("#form3").submit(
				function() {
					$.post('./test/delete.do', $(this).serialize(),
							function(status) {
								// status code 2xx (success)
								if (status.code / 100 == 2) {
									//alert('굿잡~~! 성공^^*');
									$('#form3')[0].reset();
								} else { // fail
									alert(status.message);
								}
							});
					return true;
				});

		$("#form4").submit(
				function() {
					$.post('./test/getBean.do', $(this).serialize(),
							function(status) {
								// status code 2xx (success)
								if (status.code / 100 == 2) {
									$('#form4')[0].reset();
									$('#form4').autofill(
											$.parseJSON(status.message), {
												findbyname : true,
												restrict : true
											});
								} else { // fail
									alert(status.message);
								}
							});
					return false;
				});
	});
</script>