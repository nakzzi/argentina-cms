<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<div class="px-content">

	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li class="active">앱 관리</li>
	</ol>
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"><b>App Info Update</b></span>
			</div>
			<form action=""
				class="panel-body form-horizontal form-bordered p-y-0">
				<div class="form-group panel-block">
					<div class="row">
						<label class="col-sm-4 control-label">앱 버전</label>
						<div class="col-sm-8">
							<input type="number" value="33" name="touchspin-6"
								class="form-control input-sm">
						</div>
					</div>
				</div>
				<div class="form-group panel-block">
					<div class="row">
						<label class="col-sm-4 control-label">앱 파일</label>
						<div class="col-sm-8">
							<label class="custom-file px-file"> <input type="file"
								class="custom-file-input"> <span
								class="custom-file-control form-control">Choose file...</span>
								<div class="px-file-buttons">
									<button type="button" class="btn px-file-clear">Clear</button>
									<button type="button" class="btn btn-primary px-file-browse">Browse</button>
								</div>
							</label>
						</div>
					</div>
				</div>
			</form>
			<div class="panel-footer text-right">
				<button type="button" class="btn btn-primary">Submit</button>
			</div>
		</div>
	</div>
</div>

<script src="js/custom.js"></script>
<script src="js/util.js"></script>
<script src="js/px-file.js"></script>
<script>
	// -------------------------------------------------------------------------
	// Initialize PxFile plugin

	$(function() {
		$('.custom-file').pxFile();
	});
</script>