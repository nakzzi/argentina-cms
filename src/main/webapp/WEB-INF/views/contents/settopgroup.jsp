<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="px-content">

	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li>환경 관리</li>
		<li class="active">셋탑 그룹 정보</li>
	</ol>

	<div class="page-header">
		<h1>셋탑 그룹 리스트</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-outline btn-3d">신규등록</button>
					<button type="button" class="btn btn-info btn-outline btn-3d">정보수정</button>
					<button type="button" class="btn btn-danger btn-outline btn-3d">삭제</button>
					<button type="button" class="btn btn-outline btn-3d">엑셀
						다운로드</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel box">
				<div class="box-row">
					<table id="table" data-toggle="table" data-show-refresh="true"
						data-search="true" data-pagination="true"
						data-url="./settopgroup/getlist.do">
						<thead>
							<tr>
								<th data-field="" data-checkbox="true"></th>
								<th data-field="idx" data-sortable="true">번호</th>
								<th data-field="name" data-sortable="true">이름</th>
								<th data-field="timediff" data-sortable="true">시차</th>
								<th data-field="istemp" data-sortable="true">임시지정</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>