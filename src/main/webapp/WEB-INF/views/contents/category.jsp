<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.google.gson.Gson"%>
<div class="px-content">

	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li>방송 관리</li>
		<li class="active">카테고리 관리</li>
	</ol>

	<div class="page-header">
		<h1>카테고리 관리</h1>
	</div>

	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading">
				<button type="button" class="btn btn-success btn-outline btn-3d">등록</button>
				<button type="button" class="btn btn-info btn-outline btn-3d">수정</button>
				<button type="button" class="btn btn-danger btn-outline btn-3d">삭제</button>
				<button type="button" class="btn btn-outline btn-3d">이동</button>
			</div>

			<div class="form-group panel-block" style="margin-top: 24px">
				<div id="tree" class="treeview"></div>
			</div>

		</div>
	</div>
</div>
<script src="js/bootstrap-treeview.js"></script>
<script>
	var treejson = ${treejson};
	// tree
	$('#tree').treeview({
		data : treejson, // 데이터 json형태
		levels : 99, // 트리 최대 깊이
		backColor : 'white'
	});
	$('#tree').treeview('expandAll', {
		levels : 5,
		silent : true
	});
	// Event
	$('#tree').on('nodeSelected', function(event, data) {
	});
</script>