<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="px-content">
	<ol class="breadcrumb page-breadcrumb">
		<li>홈</li>
		<li class="active">셋탑 관리</li>
	</ol>

	<div class="page-header">
		<h1>셋탑 리스트</h1>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-body">
					<button type="button" class="btn btn-success btn-outline btn-3d">신규등록</button>
					<button type="button" class="btn btn-info btn-outline btn-3d">정보수정</button>
					<button type="button" class="btn btn-danger btn-outline btn-3d">삭제</button>
					<button type="button" class="btn btn-outline btn-3d">엑셀 다운로드</button>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="panel box">
				<div class="box-row">
					<table id="table" data-toggle="table" data-show-refresh="true"
						data-search="true" data-pagination="true" data-url="./settop/getlist.do">
						<thead>
							<tr>
								<th data-field="" data-checkbox="true"></th>
								<th data-field="idx" data-sortable="true">번호</th>
								<th data-field="group" data-sortable="true">그룹명</th>
								<th data-field="id" data-sortable="true">아이디</th>
								<th data-field="username" data-sortable="true">사용자 이름</th>
								<th data-field="usernumber" data-sortable="true">연락처</th>
								<th data-field="useraddr" data-sortable="true">사용자 주소</th>
								<th data-field="macaddr" data-sortable="true">맥주소</th>
								<th data-field="regdate" data-sortable="true">등록 날짜</th>
								<th data-field="expdate" data-sortable="true">마감 날짜</th>
								<th data-field="joindate" data-sortable="true">가입 날짜</th>
								<th data-field="isactive" data-sortable="true">사용여부</th>
								<th data-field="memo" data-sortable="false">메모</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>