<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- Nav -->
<nav class="px-nav px-nav-left">
	<button type="button" class="px-nav-toggle" data-toggle="px-nav">
		<span class="px-nav-toggle-arrow"></span> <span
			class="navbar-toggle-icon"></span> <span
			class="px-nav-toggle-label font-size-11">HIDE MENU</span>
	</button>

	<ul class="px-nav-content">

		<li class="px-nav-item"><a href="./settop.do"><i
				class="px-nav-icon ion-monitor"></i><span class="px-nav-label">셋탑관리</span></a>
		</li>

		<li class="px-nav-item px-nav-dropdown"><a href="#"><i
				class="px-nav-icon ion-videocamera"></i><span class="px-nav-label">방송관리<span
					class="label label-danger">4</span></span></a>
			<ul class="px-nav-dropdown-menu">
				<li class="px-nav-item"><a href="./vod.do"><span
						class="px-nav-label">VOD 관리</span></a></li>
				<li class="px-nav-item"><a href="./live.do"><span
						class="px-nav-label">LIVE 관리</span></a></li>
				<li class="px-nav-item"><a href="./advertise.do"><span
						class="px-nav-label">광고관리</span></a></li>
				<li class="px-nav-item"><a href="./category.do"><span
						class="px-nav-label">카테고리관리</span></a></li>
			</ul></li>

		<li class="px-nav-item px-nav-dropdown"><a href="#"><i
				class="px-nav-icon ion-card"></i><span class="px-nav-label">상품권관리<span
					class="label label-danger">2</span></span></a>
			<ul class="px-nav-dropdown-menu">
				<li class="px-nav-item"><a href="./gift.do"><span
						class="px-nav-label">상품권 리스트</span></a></li>
				<li class="px-nav-item"><a href="./giftlog.do"><span
						class="px-nav-label">상품권 사용로그</span></a></li>
			</ul></li>

		<li class="px-nav-item px-nav-dropdown"><a href="#"><i
				class="px-nav-icon ion-android-person"></i><span
				class="px-nav-label">운영자관리<span class="label label-danger">2</span></span></a>

			<ul class="px-nav-dropdown-menu">
				<li class="px-nav-item"><a href="./admin.do"><span
						class="px-nav-label">운영자 리스트</span></a></li>
				<li class="px-nav-item"><a href="./authority.do"><span
						class="px-nav-label">권한 설정</span></a></li>
			</ul></li>

		<li class="px-nav-item px-nav-dropdown"><a href="#"><i
				class="px-nav-icon ion-ios-gear"></i><span class="px-nav-label">환경관리<span
					class="label label-danger">5</span></span></a>
			<ul class="px-nav-dropdown-menu">
				<li class="px-nav-item"><a href="./servergroup.do"><span
						class="px-nav-label">서버 그룹 정보</span></a></li>
				<li class="px-nav-item"><a href="./serverinfo.do"><span
						class="px-nav-label">서버 정보</span></a></li>
				<li class="px-nav-item"><a href="./settopgroup.do"><span
						class="px-nav-label">셋탑 그룹 정보</span></a></li>
				<li class="px-nav-item"><a href="./mapping.do"><span
						class="px-nav-label">서버-셋탑 그룹 매핑</span></a></li>
				<li class="px-nav-item"><a href="./app.do"><span
						class="px-nav-label">앱관리</span></a></li>
			</ul></li>

		<li class="px-nav-item"><a href="./notice.do"><i
				class="px-nav-icon ion-ios-paper"></i><span class="px-nav-label">공지사항관리</span></a>
		</li>

		<li class="px-nav-item"><a href="./test.do"><i
				class="px-nav-icon ion-ios-paper"></i><span class="px-nav-label">테스트</span></a>
		</li>
	</ul>
</nav>