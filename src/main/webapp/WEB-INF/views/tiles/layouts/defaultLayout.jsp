<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<title><tiles:getAsString name="title" /></title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

<link href="css/fonts.css" rel="stylesheet" type="text/css">
<link href="css/ionicons.min.css" rel="stylesheet" type="text/css">

<!-- Core stylesheets -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/pixeladmin.min.css" rel="stylesheet" type="text/css">
<link href="css/widgets.min.css" rel="stylesheet" type="text/css">

<!-- Theme -->
<link href="css/themes/candy-blue.min.css" rel="stylesheet"
	type="text/css">

<!-- Bootstrap Table -->
<link href="css/bootstrap-table.min.css" rel="stylesheet"
	type="text/css">

<!-- Pace.js -->
<script src="pace/pace.min.js"></script>
<style>
::-webkit-scrollbar {
	display: none;
}
</style>
</head>

<body>
	<script src="js/require.js"></script>
	<script src="js/requirejs-config.js"></script>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-table.min.js"></script>
	<script src="js/bootstrap-table-ko-KR.min.js"></script>
	<script src="js/main.js"></script>

	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	<tiles:insertAttribute name="footer" />
</body>
</html>