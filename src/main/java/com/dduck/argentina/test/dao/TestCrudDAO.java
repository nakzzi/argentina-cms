package com.dduck.argentina.test.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.test.vo.TestBean;

/**
 * TestCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class TestCrudDAO extends Crud<TestBean> {
	private static final String TABLENAME = "test"; 
	
	/**
	 * @param tableName
	 */
	public TestCrudDAO() {
		super(TABLENAME);
	}
}
