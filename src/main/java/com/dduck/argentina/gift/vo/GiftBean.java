package com.dduck.argentina.gift.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * GiftBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class GiftBean {
	Integer idx, settopgroup, validdatecount;
	String regdate, code, pin, status, usagedate, madeby, apply, user;
}
