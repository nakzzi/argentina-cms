package com.dduck.argentina.gift.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.gift.vo.GiftBean;

/**
 * GiftCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class GiftCrudDAO extends Crud<GiftBean> {
	private static final String TABLENAME = "gift"; 
	
	/**
	 * @param tableName
	 */
	public GiftCrudDAO() {
		super(TABLENAME);
	}
}
