package com.dduck.argentina.notice.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.notice.vo.NoticeBean;

/**
 * NoticeCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class NoticeCrudDAO extends Crud<NoticeBean> {
	private static final String TABLENAME = "notice"; 
	
	/**
	 * @param tableName
	 */
	public NoticeCrudDAO() {
		super(TABLENAME);
	}
}
