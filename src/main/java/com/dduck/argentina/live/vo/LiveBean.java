package com.dduck.argentina.live.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * LiveBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class LiveBean {
	Integer idx, channel, sequence;
	String type, name, moddate, path;
}
