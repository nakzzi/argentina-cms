package com.dduck.argentina.live.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.live.vo.LiveBean;

/**
 * LiveCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class LiveCrudDAO extends Crud<LiveBean> {
	private static final String TABLENAME = "live"; 
	
	/**
	 * @param tableName
	 */
	public LiveCrudDAO() {
		super(TABLENAME);
	}
}
