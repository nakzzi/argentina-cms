package com.dduck.argentina.server.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.server.vo.ServerInfoBean;

/**
 * HomeCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class ServerInfoCrudDAO extends Crud<ServerInfoBean> {
	private static final String TABLENAME = "server"; 
	
	/**
	 * @param tableName
	 */
	public ServerInfoCrudDAO() {
		super(TABLENAME);
	}
}
