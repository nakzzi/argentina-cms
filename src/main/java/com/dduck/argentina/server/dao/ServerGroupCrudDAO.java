package com.dduck.argentina.server.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.server.vo.ServerGroupBean;

/**
 * ServerGroupCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class ServerGroupCrudDAO extends Crud<ServerGroupBean> {
	private static final String TABLENAME = "servergroup"; 
	
	/**
	 * @param tableName
	 */
	public ServerGroupCrudDAO() {
		super(TABLENAME);
	}
}
