package com.dduck.argentina.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ServerInfoBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class ServerInfoBean {
	Integer idx, servergroup;
	String name, type, ip, url, port, isusage;
}
