package com.dduck.argentina.category.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import com.dduck.argentina.category.vo.CategoryBean;
import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.common.sql.Config;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * CategoryCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class CategoryCrudDAO extends Crud<CategoryBean> {
	private static final String TABLENAME = "category"; 
	
	/**
	 * @param tableName
	 */
	public CategoryCrudDAO() {
		super(TABLENAME);
	}
	
	/**
	 * 계층적 트리로 정렬된 리스트 반환
	 * 
	 * @return	정렬된 리스트
	 */
	public List<CategoryBean> getSortedList() throws Exception {
		List<CategoryBean> list = new Gson().fromJson(super.getList(), 
				new TypeToken<List<CategoryBean>>() {}.getType());
		return sortHierarchicalTree(list, null);
	}
	
	/**
	 * 계층적 트리로 정렬된 리스트를 json형태로 반환
	 * 
	 * @return 정렬된 리스트의 json형태
	 */
	public String getSortedJson() throws Exception {
		return new Gson().toJson(getSortedList());
	}
	
	/**
	 * 자식노드 반환
	 * 
	 * pid에 해당하는 인덱스와 일치하는 자식노드를 반환 
	 * 
	 * @param pid	기준이 되는 부모 인덱스
	 * @return	자식노드 리스트
	 */
	public List<CategoryBean> getChildren(Integer pid) throws Exception {
		List<CategoryBean> list = new Gson().fromJson(super.getList(), 
				new TypeToken<List<CategoryBean>>() {}.getType());
		return sortHierarchicalTree(list, pid);
	}
	
	/**
	 * 부모노드 반환
	 * 
	 * @param cid	기준이 되는 자식 인덱스
	 * @return	부모노드
	 */
	public CategoryBean getParent(Integer cid) throws Exception {
		CategoryBean bean = new Gson().fromJson(super.getBean(cid), 
				new TypeToken<CategoryBean>() {}.getType());
		CategoryBean result = new Gson().fromJson(super.getBean(bean.getParent()), 
				new TypeToken<CategoryBean>() {}.getType());
		return result;
	}
	
	/**
	 * 계층적 트리로 정렬 
	 * 
	 * list내에서 pid를 부모 노드로 하는 자식 노드를 찾아 자식에 추가(nodes)한 후 정렬된 리스트를 반환  
	 * 
	 * INPUT list
	 * 		[id : 1-2-2, pid : 1-2, sequence : 2]
	 * 		[id : 1-1, pid : 1, sequence : 1]
	 * 		[id : 1, pid : null, sequence : 1]
	 * 		[id : 1-2-1, pid : 1-2, sequence : 1]
	 * 		[id : 1-2, pid : 1, sequence : 2]
	 * 		[id : 1-1-1, pid : 1-1, sequence : 1]
	 * 
	 * OUTPUT list
	 * 		[id : 1, pid : null, sequence : 1]
	 * 				└─ [id : 1-1, pid : 1, sequence : 1]
	 *  										└─ [id : 1-1-1, pid : 1-1, sequence : 1]
	 * 				└─ [id : 1-2, pid : 1, sequence : 2]
	 * 											└─ [id : 1-2-1, pid : 1-2, sequence : 1]
	 * 											└─ [id : 1-2-2, pid : 1-2, sequence : 2]
	 * 
	 * @param list	정렬할 리스트
	 * @param pid	부모 노드의 인덱스
	 * @return	계층적으로 정렬된 리스트
	 */
	private List<CategoryBean> sortHierarchicalTree(List<CategoryBean> list, Integer pid) {
		List<CategoryBean> root = getChildren(list, pid); // 새로운 root 생성
		for(CategoryBean child : root) {
			child.setText(child.getName());
			child.setNodes(sortHierarchicalTree(list, child.getIdx()));	// 재귀지점
		}
		return root.isEmpty() ? null : root;
	}
	
	/**
	 * 자식노드 반환
	 * 
	 * 리스트형태의 트리구조를 받아 pid에 해당하는 인덱스를 list내 부모노드 인덱스로 갖는 노드와 비교하여 일치하는 자식노드를 반환 
	 * 
	 * @param list	트리구조의 리스트
	 * @param pid	기준이 되는 부모 인덱스
	 * @return	자식노드 리스트
	 */
	private List<CategoryBean> getChildren(List<CategoryBean> list, Integer pid) {
		List<CategoryBean> children = new ArrayList<>();
		for(CategoryBean bean : list) {
			if(pid == null) {
				if(bean.getParent() == null) {
					children.add(bean);
				}
			} else {
				if(bean.getParent() == null) continue;
				if(pid.intValue() == bean.getParent().intValue()) {
					children.add(bean);
				}
			}
		}
		children.sort(new CategoryComp());
		return children;
	}
	
	private final class CategoryComp implements Comparator<CategoryBean> {
		@Override
		public int compare(CategoryBean o1, CategoryBean o2) {
			if(o1.getSequence() > o2.getSequence()) return 1;
			else if(o1.getSequence() < o2.getSequence()) return -1;
			else return 0;
		}
	}
	
	public synchronized Integer getNextSequence(Integer parent) throws Exception {
		List<Map<String, Object>> listOfMaps = null;
		
		Integer next = null;
		Connection conn = Config.getInstance().sqlLogin();
		try {
			QueryRunner queryRunner = new QueryRunner();  
			listOfMaps = queryRunner.query(conn, 
					"SELECT MAX(A.sequence)+1 as 'NEXT' FROM (SELECT * FROM category WHERE parent" + (parent==null ? " is null" : " = " + parent) + ") A;",
					new MapListHandler());
		} finally { 
			DbUtils.closeQuietly(conn);
		}
		Object obj = listOfMaps.get(0).get("NEXT");
		if(obj != null) {
			next = Integer.parseInt(obj+"");
		}
		return next; 
	}
	
	@Override
	public void insert(CategoryBean bean) throws Exception {
		bean.setSequence(getNextSequence(bean.getParent()));
		super.insert(bean);
	}
}
