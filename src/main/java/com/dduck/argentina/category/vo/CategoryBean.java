package com.dduck.argentina.category.vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * CategoryBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class CategoryBean {
	Integer idx, parent, sequence;
	String name;

	String text;
	List<CategoryBean> nodes;
}
