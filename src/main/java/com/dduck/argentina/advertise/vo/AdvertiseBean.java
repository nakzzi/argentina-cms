package com.dduck.argentina.advertise.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * AdvertiseBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class AdvertiseBean {
	Integer idx, frequency;
	String name, filename, regdate;
}
