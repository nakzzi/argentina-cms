package com.dduck.argentina.advertise.dao;

import com.dduck.argentina.advertise.vo.AdvertiseBean;
import com.dduck.argentina.common.pattern.dao.Crud;

/**
 * AdvertiseCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class AdvertiseCrudDAO extends Crud<AdvertiseBean> {
	private static final String TABLENAME = "advertise"; 
	
	/**
	 * @param tableName
	 */
	public AdvertiseCrudDAO() {
		super(TABLENAME);
	}
}
