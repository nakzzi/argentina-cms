package com.dduck.argentina.app.dao;

import com.dduck.argentina.app.vo.AppBean;
import com.dduck.argentina.common.pattern.dao.Crud;

/**
 * AppCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class AppCrudDAO extends Crud<AppBean> {
	private static final String TABLENAME = "app"; 
	
	/**
	 * @param tableName
	 */
	public AppCrudDAO() {
		super(TABLENAME);
	}
}
