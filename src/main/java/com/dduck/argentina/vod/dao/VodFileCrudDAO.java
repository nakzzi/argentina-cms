package com.dduck.argentina.vod.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.vod.vo.VodFileBean;

/**
 * VodFileCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class VodFileCrudDAO extends Crud<VodFileBean> {
	private static final String TABLENAME = "vodfile"; 
	
	/**
	 * @param tableName
	 */
	public VodFileCrudDAO() {
		super(TABLENAME);
	}
}
