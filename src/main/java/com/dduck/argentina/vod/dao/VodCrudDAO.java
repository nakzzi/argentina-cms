package com.dduck.argentina.vod.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.vod.vo.VodBean;

/**
 * VodCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class VodCrudDAO extends Crud<VodBean> {
	private static final String TABLENAME = "vod"; 
	
	/**
	 * @param tableName
	 */
	public VodCrudDAO() {
		super(TABLENAME);
	}
}
