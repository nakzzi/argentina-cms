package com.dduck.argentina.vod.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dduck.argentina.common.pattern.vo.Status;
import com.dduck.argentina.vod.dao.VodCrudDAO;
import com.dduck.argentina.vod.vo.VodBean;

@Controller
public class VodController {
	
	private static final Logger logger = LoggerFactory.getLogger(VodController.class);
	private static final String TABLENAME = "vod"; 
	
	private VodCrudDAO getDefaultDAO() {
		return new VodCrudDAO();
	}
	
	@RequestMapping(value = "/"+TABLENAME+".do")
	public ModelAndView openIndexPage() throws Exception {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("contents/" + TABLENAME);
		return model;
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/getlist.do", 
			produces = "application/json; charset=utf-8")
	public @ResponseBody String getList() throws Exception {
		
		String list = getDefaultDAO().getList();
		return list;
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/getBean.do", 
			produces = "application/json; charset=utf-8")
	public @ResponseBody Status getBean(
			@RequestParam(value = "idx", required = true) Integer idx) throws Exception {

		String bean = getDefaultDAO().getBean(idx);
		return new Status(200, bean);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/insert.do")
	public @ResponseBody Status insert(VodBean bean) throws Exception {
		
		getDefaultDAO().insert(bean);
		return new Status(200);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/update.do")
	public @ResponseBody Status update(VodBean bean) throws Exception {
		
		getDefaultDAO().update(bean);
		return new Status(200);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/delete.do")
	public @ResponseBody Status delete(
			@RequestParam(value = "idx", required = true) Integer idx) throws Exception {

		getDefaultDAO().delete(idx);
		return new Status(200);
	}
	
	
	@ExceptionHandler
	public @ResponseBody Status handle(Exception e) {
		logger.info("Exception Handler - " + e.getMessage());
		return new Status(400, "Exception handled!" + e.getMessage());
	}
	
}
