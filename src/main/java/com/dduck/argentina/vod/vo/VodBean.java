package com.dduck.argentina.vod.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * VodBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class VodBean {
	Integer idx, category, sequence;
	String type, title, moddate, status, shortdesc, desc, imgpath;
}
