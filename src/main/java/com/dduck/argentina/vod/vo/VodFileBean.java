package com.dduck.argentina.vod.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * VodFileBean
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class VodFileBean {
	Integer idx, idxvod, sequence;
	String title, path, moddate;
}
