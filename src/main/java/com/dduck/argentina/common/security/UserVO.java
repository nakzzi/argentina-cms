package com.dduck.argentina.common.security;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data @Getter @Setter @ToString
public class UserVO {
    private String id, password;
}
