package com.dduck.argentina.common.security.xauth;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import com.dduck.argentina.common.security.model.UserService;
import com.dduck.argentina.common.security.model.domain.User;

/**
 * AuthenticationTokenProcessingFilter
 * 
 * @version 1.0 [2018. 4. 2.]
 * @author Choi
 */
@Component
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationTokenProcessingFilter.class);
	
	@Autowired 
	UserService userService;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = this.getAsHttpRequest(request);
		String authToken = this.extractAuthTokenFromRequest(httpRequest); 
		String userName = TokenUtils.getUserNameFromToken(authToken); 
		if (userName != null) { 
			User user = userService.loadUserByUsername(userName); 
			if (TokenUtils.validateToken(authToken, user)) { 
				UsernamePasswordAuthenticationToken authentication 
					= new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()); 
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest)); 
				SecurityContextHolder.getContext().setAuthentication(authentication); 
			} 
		} chain.doFilter(request, response);

	}

	private HttpServletRequest getAsHttpRequest(ServletRequest request) { 
		if (!(request instanceof HttpServletRequest)) { 
			throw new RuntimeException("Expecting an HTTP request"); 
		} return (HttpServletRequest) request; 
	} 
	
	private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) { 
		String authToken = httpRequest.getHeader("X-Auth-Token"); 
		return authToken; 
	}

}
