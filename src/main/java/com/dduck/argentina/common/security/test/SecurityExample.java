package com.dduck.argentina.common.security.test;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * SecurityExample
 * 
 * @version 1.0 [2018. 3. 30.]
 * @author Choi
 */
public class SecurityExample {
	static AuthenticationManager authenticationManager = new SAuthenticationManager();
	
	public static void main(String...args) {
		String username = "";
		String password = "";
		
		Authentication req = new UsernamePasswordAuthenticationToken(username, password);
		Authentication result = authenticationManager.authenticate(req);
		SecurityContextHolder.getContext().setAuthentication(result);
		
		System.out.println(SecurityContextHolder.getContext().getAuthentication());
	}
}
