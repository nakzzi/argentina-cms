package com.dduck.argentina.common.security.model;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dduck.argentina.admin.dao.AdminCrudDAO;
import com.dduck.argentina.admin.vo.AdminBean;
import com.dduck.argentina.common.security.model.domain.Role;
import com.dduck.argentina.common.security.model.domain.User;

@Service
public class UserService implements UserDetailsService {

    @Override
    public User loadUserByUsername(final String username) throws UsernameNotFoundException {
    	User user = new User();
    	AdminBean adminBean = new AdminCrudDAO().getMember(username);
    	
    	if (adminBean == null) {
    		throw new UsernameNotFoundException("접속자 정보를 찾을 수 없습니다.");
    	} else {
    		
        	user.setUsername(adminBean.getUsername());
        	user.setPassword(adminBean.getPassword());
        	user.setNickname(adminBean.getNickname());
        	Role role = new Role();
        	role.setName("ROLE_"+adminBean.getRole());
        	List<Role> roles = new ArrayList<Role>();
        	roles.add(role);
        	
        	user.setAuthorities(roles);
    	}
    	
    	return user;
    }
}
