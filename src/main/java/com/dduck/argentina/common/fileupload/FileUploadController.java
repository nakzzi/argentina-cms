package com.dduck.argentina.common.fileupload;

import java.io.File;
import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.dduck.argentina.common.ajax.AjaxUtils;
import com.dduck.argentina.common.path.dao.PathCrudDAO;

@Controller
public class FileUploadController {

	static String defaultPath;
	
	public FileUploadController() {
		defaultPath = new PathCrudDAO().getValue("upload_default_path");
	}
	
	@RequestMapping(value = "/upload")
	public ModelAndView upload(
			ModelAndView model,
			@RequestParam MultipartFile file,
			@RequestParam(value = "path", required = false) String path) throws Exception {
		
		File dest = new File( (path==null?defaultPath+"/":path+(path.endsWith("/")?"":"/"))+file.getOriginalFilename() );
		try {
			file.transferTo(dest);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		
		model.addObject("message", "File '" + file.getOriginalFilename() + "' uploaded successfully");
		model.setViewName("contents/test");
		return model;
	}
	
	@ModelAttribute
	public void ajaxAttribute(WebRequest request, Model model) {
		model.addAttribute("ajaxRequest", AjaxUtils.isAjaxRequest(request));
	}
}
