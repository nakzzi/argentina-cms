package com.dduck.argentina.common.path.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import com.dduck.argentina.admin.vo.AdminBean;
import com.dduck.argentina.common.path.vo.PathBean;
import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.common.security.SQLInjectionDefender;
import com.dduck.argentina.common.sql.Config;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * PathCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class PathCrudDAO extends Crud<PathBean> {
	private static final String TABLENAME = "common_path"; 
	
	/**
	 * @param tableName
	 */
	public PathCrudDAO() {
		super(TABLENAME);
	}
	
	public String getValue(String key) {
		Connection conn = Config.getInstance().sqlLogin();
		PathBean result = null;
		List<Map<String, Object>> listOfMaps = null;
		try {
			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(conn, "SELECT * FROM " + TABLENAME + " WHERE " + TABLENAME + ".key = ?;", 
						new MapListHandler(), key);
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(conn);
		}
		
		if(!listOfMaps.isEmpty()) {
			Gson gson = new Gson();
			result = new Gson().fromJson(gson.toJson(listOfMaps.get(0)), 
					new TypeToken<PathBean>() {}.getType());
		}
		return result.getValue();
	}
}
