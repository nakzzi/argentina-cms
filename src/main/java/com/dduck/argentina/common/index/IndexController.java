package com.dduck.argentina.common.index;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class IndexController {
	
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String openDefaultPage() {
		logger.info("Welcome home!");
		
		return "contents/home";
	}
	
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String openIndexPage() {
		return "contents/home";
	}
}
