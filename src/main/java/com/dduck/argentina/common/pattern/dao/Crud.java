package com.dduck.argentina.common.pattern.dao;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;

import com.dduck.argentina.common.security.SQLInjectionDefender;
import com.dduck.argentina.common.sql.Config;
import com.google.gson.Gson;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Crud
 * 
 * @version 1.0 [2018. 3. 28.]
 * @author Choi
 */
@Data @AllArgsConstructor
public class Crud<T> {
	
	/**
	 * 사전에 정의 된 table name
	 */
	private String tableName;
	
	/**
	 * 사전에 정의 된 table index의 column name
	 */
	private static final String IDX = "idx"; 
	
	/**
	 * 모든 컬럼 반환
	 * 
	 * @return db내의 모든 컬럼을 bean으로 변환 후 json형태로 반환
	 */
	public String getList() throws Exception {
		return getList("");
	}
	public String getList(String tail) throws Exception {
		
		Connection conn = Config.getInstance().sqlLogin();
		List<Map<String, Object>> listOfMaps = null;
		try {
			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(conn, "SELECT * FROM " + tableName + " " + 
						(SQLInjectionDefender.isSQLInjection(tail) ? "" : (tail==null?"":tail)) + ";", 
						new MapListHandler());
		} finally {
			DbUtils.closeQuietly(conn);
		}
		Gson gson = new Gson();
		return gson.toJson(listOfMaps);
	}
	
	/**
	 * idx에 해당하는 컬럼 반환 
	 * 
	 * @param idx 반환 할 컬럼의 인덱스
	 * @return idx에 해당하는 컬럼이 검색되었다면 해당 컬럼을 bean형태로 반환, 그렇지 않으면 null반환
	 */
	public String getBean(Integer idx) throws Exception {
		return getBean(idx, "");
	}
	public String getBean(Integer idx, String tail) throws Exception {
		Connection conn = Config.getInstance().sqlLogin();
		String result = null;
		List<Map<String, Object>> listOfMaps = null;
		try {
			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(conn, "SELECT * FROM " + tableName + " WHERE " + IDX + " = ? " + 
						(SQLInjectionDefender.isSQLInjection(tail) ? "" : (tail==null?"":tail)) + ";", 
						new MapListHandler(), idx);
		} finally {
			DbUtils.closeQuietly(conn);
		}
		
		if(!listOfMaps.isEmpty()) {
			result = new Gson().toJson(listOfMaps.get(0));
		}
		return result;
	}
	
	/**
	 * 삽입 연산
	 * 
	 * @param bean 삽입 할 컬럼의 내용
	 */
	public void insert(T bean) throws Exception {
		Connection conn = Config.getInstance().sqlLogin();
		try {
			QueryRunner queryRunner = new QueryRunner();
			Field[] fields = bean.getClass().getDeclaredFields();
			Object[] params = new Object[fields.length];
			StringBuilder ps = new StringBuilder();
			
			for(int i=0; i<fields.length; i++) {
				Field field = fields[i];
				field.setAccessible(true);
				params[i] = field.get(bean);
				ps.append("?");
				if(i < fields.length-1) {
					ps.append(",");
				}
			}
			queryRunner.update(conn, "INSERT INTO " + tableName + " VALUES(" + ps + ")", params);
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}
	
	/**
	 * 수정 연산
	 * 
	 * @param bean 수정 할 컬럼의 내용 (idx에는 수정 할 인덱스가 포함)
	 */
	public void update(T bean) throws Exception {
		Connection conn = Config.getInstance().sqlLogin();
		try {
			QueryRunner queryRunner = new QueryRunner();
			Field[] fields = bean.getClass().getDeclaredFields();
			Object[] params = new Object[fields.length];
			StringBuilder ps = new StringBuilder();
			
			for(int i=0,j=0; i<fields.length; i++,j++) {
				Field field = fields[i];
				field.setAccessible(true);
				if(field.getName().equals(IDX)) {
					j--;
					continue;
				}
				params[j] = field.get(bean);
				ps.append(tableName+"."+field.getName()+"=? ");
				if(i < fields.length-1) {
					ps.append(",");
				}
			}
			params[fields.length-1] = fields[0].get(bean);
			queryRunner.update(conn, "UPDATE " + tableName + " SET " + ps + "WHERE " + IDX + " = ?;", 
					params); 
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}
	
	/**
	 * 삭제 연산
	 * 
	 * @param idx 삭제할 컬럼의 idx
	 */
	public void delete(Integer idx) throws Exception {
		Connection conn = Config.getInstance().sqlLogin();
		try {
			new QueryRunner().update(conn, "DELETE FROM " + tableName + " WHERE " + IDX + " = ?;", idx);
		} finally {
			DbUtils.closeQuietly(conn);
		}
	}
}
