package com.dduck.argentina.admin.controller;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.dduck.argentina.admin.dao.AdminCrudDAO;
import com.dduck.argentina.admin.vo.AdminBean;
import com.dduck.argentina.common.pattern.vo.Status;
import com.dduck.argentina.common.security.crypt.RSA;

@Controller
public class AdminController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
	private static final String TABLENAME = "admin"; 
	
	private AdminCrudDAO getDefaultDAO() {
		return new AdminCrudDAO();
	}

	@RequestMapping(value = "/"+TABLENAME+".do")
	public ModelAndView openIndexPage() throws Exception {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("contents/" + TABLENAME);
		return model;
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/getlist.do", 
			produces = "application/json; charset=utf-8")
	public @ResponseBody String getList() throws Exception {
		
		String list = getDefaultDAO().getList();
		return list;
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/getBean.do", 
			produces = "application/json; charset=utf-8")
	public @ResponseBody Status getBean(
			@RequestParam(value = "idx", required = true) Integer idx) throws Exception {

		String bean = getDefaultDAO().getBean(idx);
		return new Status(200, bean);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/insert.do")
	public @ResponseBody Status insert(
			AdminBean bean,
			HttpServletRequest request) throws Exception {
		
		bean.setPassword(decryptRSA(request, bean.getPassword()));
		getDefaultDAO().insert(bean);
		return new Status(200);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/update.do")
	public @ResponseBody Status update(
			AdminBean bean,
			HttpServletRequest request) throws Exception {
		
		bean.setPassword(decryptRSA(request, bean.getPassword()));
		getDefaultDAO().update(bean);
		return new Status(200);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/delete.do")
	public @ResponseBody Status delete(
			@RequestParam(value = "idx", required = true) Integer idx) throws Exception {

		getDefaultDAO().delete(idx);
		return new Status(200);
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/signup.do")
	public @ResponseBody ModelAndView insertPage(
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		setRSA(request, 1024);
		model.setViewName("signup");
		return model;
	}
	
	@RequestMapping(value = "/"+TABLENAME+"/edit.do")
	public @ResponseBody ModelAndView updatePage(
			HttpServletRequest request) throws Exception {
		
		ModelAndView model = new ModelAndView();
		setRSA(request, 1024);
		model.setViewName("edit");
		return model;
	}
	
	@ExceptionHandler
	public @ResponseBody Status handle(Exception e) {
		logger.info("Exception Handler - " + e.getMessage());
		return new Status(400, "Exception handled!" + e.getMessage());
	}
	
	private String decryptRSA(HttpServletRequest request, String cipher) {
		String plainText = null;
		PrivateKey privateKey = (PrivateKey) request.getSession().getAttribute("__rsaPrivateKey__");
		if (privateKey == null) {
			throw new RuntimeException("암호화 비밀키 정보를 찾을 수 없습니다.");
		} else {
			RSA rsa = new RSA(); 
			plainText = rsa.decrypt(privateKey, cipher);
		}
		return plainText;
	}
	
	private void setRSA(HttpServletRequest request, final int KEY_SIZE) throws Exception {
    	request.setCharacterEncoding("UTF-8");

    	HttpSession session = request.getSession();
    	
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(KEY_SIZE);

        KeyPair keyPair = generator.genKeyPair();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");

        PublicKey publicKey = keyPair.getPublic();
        PrivateKey privateKey = keyPair.getPrivate();

        session.setAttribute("__rsaPrivateKey__", privateKey);

        RSAPublicKeySpec publicSpec = 
        		(RSAPublicKeySpec) keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);

        String publicKeyModulus = publicSpec.getModulus().toString(16);
        String publicKeyExponent = publicSpec.getPublicExponent().toString(16);

        request.setAttribute("publicKeyModulus", publicKeyModulus);
        request.setAttribute("publicKeyExponent", publicKeyExponent);
	}
}
