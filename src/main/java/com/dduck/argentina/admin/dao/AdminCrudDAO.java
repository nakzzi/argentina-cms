package com.dduck.argentina.admin.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.dduck.argentina.admin.vo.AdminBean;
import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.common.security.password.PasswordEncoding;
import com.dduck.argentina.common.security.password.SHAPasswordEncoder;
import com.dduck.argentina.common.sql.Config;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * AdminCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class AdminCrudDAO extends Crud<AdminBean> {
	private static final String TABLENAME = "member"; 
	
	/**
	 * @param tableName
	 */
	public AdminCrudDAO() {
		super(TABLENAME);
	}
	
	public AdminBean getMember(String username) {
		Connection conn = Config.getInstance().sqlLogin();
		AdminBean result = null;
		List<Map<String, Object>> listOfMaps = null;
		try {
			QueryRunner queryRunner = new QueryRunner();
			listOfMaps = queryRunner.query(conn, "SELECT * FROM " + TABLENAME + " WHERE username = ?;", 
						new MapListHandler(), username);
		} catch(SQLException e) {
			e.printStackTrace();
		} finally {
			DbUtils.closeQuietly(conn);
		}
		
		if(!listOfMaps.isEmpty()) {
			Gson gson = new Gson();
			result = new Gson().fromJson(gson.toJson(listOfMaps.get(0)), 
					new TypeToken<AdminBean>() {}.getType());
		}
		return result;
	}
	
	@Override
	public void insert(AdminBean bean) throws Exception {
		SHAPasswordEncoder shaPasswordEncoder = new SHAPasswordEncoder(512);
		shaPasswordEncoder.setEncodeHashAsBase64(true);
		PasswordEncoding pwEncoder = new PasswordEncoding(shaPasswordEncoder);
		PasswordEncoding bCryptEncoder = new PasswordEncoding(new BCryptPasswordEncoder());
		
		String bCrypt = bCryptEncoder.encode(pwEncoder.encode(bean.getPassword()));
		bean.setPassword(bCrypt);
		super.insert(bean);
	}
}
