package com.dduck.argentina.admin.dao;

import com.dduck.argentina.admin.vo.AuthorityBean;
import com.dduck.argentina.common.pattern.dao.Crud;

/**
 * AuthorityCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class AuthorityCrudDAO extends Crud<AuthorityBean> {
	private static final String TABLENAME = "authority"; 
	
	/**
	 * @param tableName
	 */
	public AuthorityCrudDAO() {
		super(TABLENAME);
	}
}
