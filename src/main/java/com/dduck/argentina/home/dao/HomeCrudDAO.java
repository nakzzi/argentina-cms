package com.dduck.argentina.home.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.home.vo.HomeBean;

/**
 * HomeCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class HomeCrudDAO extends Crud<HomeBean> {
	private static final String TABLENAME = "home"; 
	
	/**
	 * @param tableName
	 */
	public HomeCrudDAO() {
		super(TABLENAME);
	}
}
