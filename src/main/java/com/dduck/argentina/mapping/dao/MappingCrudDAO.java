package com.dduck.argentina.mapping.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.mapping.vo.MappingBean;

/**
 * MappingCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class MappingCrudDAO extends Crud<MappingBean> {
	private static final String TABLENAME = "settopgroup_servergroup"; 
	
	/**
	 * @param tableName
	 */
	public MappingCrudDAO() {
		super(TABLENAME);
	}
}
