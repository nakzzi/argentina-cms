package com.dduck.argentina.settop.dao;

import java.util.List;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.settop.vo.SettopBean;
import com.dduck.argentina.settop.vo.TableStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * SettopCrudDAO
 * 
 * @version 1.0 [2018. 3. 28.]
 * @author Choi
 */
public class SettopCrudDAO extends Crud<SettopBean> {
	private static final String TABLENAME = "settop"; 
	
	/**
	 * @param tableName
	 */
	public SettopCrudDAO() {
		super(TABLENAME);
	}
	
	@Override
	public String getList() throws Exception {
		/*
		 * isactive 설정
		 */
		Gson gson = new Gson();
		List<SettopBean> list = gson.fromJson(super.getList(), 
				new TypeToken<List<SettopBean>>() {}.getType());
		
		for(SettopBean bean : list) {
			convert(bean, false);
		}
		return gson.toJson(list);
	}
	
	@Override
	public void insert(SettopBean bean) throws Exception {
		convert(bean, true);
		super.insert(bean);
	}

	/**
	 * IsActive 설정
	 * 
	 * @param bean	변경할 SettopBean
	 * @param isGoingDB	true:db로 삽입되는 데이타에 해당, false:db에서 추출되는 데이타에 해당
	 * @return	변경된 SettopBean
	 * @see TableStatus
	 */
	private void convert(SettopBean bean, boolean isGoingDB) {
		if( isGoingDB ) {
			if(bean.getIsactive() == null) {
				bean.setIsactive(TableStatus.N.getDbValue());
			} else if(!bean.getIsactive().equals(TableStatus.Y.getVisibleValue())) {
				bean.setIsactive(TableStatus.N.getDbValue());
			} else {
				bean.setIsactive(TableStatus.Y.getDbValue());
			}
		} else {
			if(bean.getIsactive().equals(TableStatus.Y.getDbValue())) {
				bean.setIsactive(TableStatus.Y.getVisibleValue());
			} else {
				bean.setIsactive(TableStatus.N.getVisibleValue());
			}
		}
	}
}
