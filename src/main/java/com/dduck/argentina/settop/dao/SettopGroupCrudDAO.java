package com.dduck.argentina.settop.dao;

import com.dduck.argentina.common.pattern.dao.Crud;
import com.dduck.argentina.settop.vo.SettopGroupBean;

/**
 * SettopGroupCrudDAO
 * 
 * @version 1.0 [2018. 3. 29.]
 * @author Choi
 */
public class SettopGroupCrudDAO extends Crud<SettopGroupBean> {
	private static final String TABLENAME = "settopgroup"; 
	
	/**
	 * @param tableName
	 */
	public SettopGroupCrudDAO() {
		super(TABLENAME);
	}
}
