package com.dduck.argentina.settop.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * SettopBean
 * 
 * @version 1.0 [2018. 3. 26.]
 * @author Choi
 */
@Data @AllArgsConstructor @NoArgsConstructor
public class SettopBean {
	Integer idx, groupname;
	String id, regdate, expdate, joindate, isactive, macaddr, username, usernumber, useraddr, memo;
}
