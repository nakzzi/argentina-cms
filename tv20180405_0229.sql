CREATE DATABASE  IF NOT EXISTS `db_sevenstar` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_sevenstar`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: db_sevenstar
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertise`
--

DROP TABLE IF EXISTS `advertise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertise` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `filename` varchar(150) DEFAULT NULL,
  `frequency` int(11) DEFAULT NULL,
  `regdate` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertise`
--

LOCK TABLES `advertise` WRITE;
/*!40000 ALTER TABLE `advertise` DISABLE KEYS */;
INSERT INTO `advertise` VALUES (1,'2018-07','poster_151476554.jpg',1,'2018-01-02 00:12:23');
/*!40000 ALTER TABLE `advertise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autority`
--

DROP TABLE IF EXISTS `autority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autority` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `type` int(3) unsigned NOT NULL COMMENT 'e.g.\n0 : settop\n1 : vod\n2 : live\n3 : advertise\n4 : category\n5 : gift\n6 : giftlog\n7 : admin\n8 : authority\n9 : servergroup\n10 : serverinfo\n11 : settopgroup\n12 : mapping\n13 : app\n14 : notice',
  `authority` int(3) unsigned NOT NULL COMMENT '0 : no permission\n1 : accept permission\nfirst arg : view authority\nsecond arg : create, update, delete authority',
  PRIMARY KEY (`idx`),
  KEY `autority_fk_username_idx` (`username`),
  CONSTRAINT `autority_fk_username` FOREIGN KEY (`username`) REFERENCES `member` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autority`
--

LOCK TABLES `autority` WRITE;
/*!40000 ALTER TABLE `autority` DISABLE KEYS */;
INSERT INTO `autority` VALUES (1,'admin',0,11),(2,'admin',1,11),(3,'admin',2,11),(4,'admin',3,11),(5,'admin',4,11),(6,'admin',5,11),(7,'admin',6,11),(8,'admin',7,11),(9,'admin',8,11),(10,'admin',9,11),(11,'admin',10,11),(12,'admin',11,11),(13,'admin',12,11),(14,'admin',13,11),(15,'admin',14,11);
/*!40000 ALTER TABLE `autority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `sequence` int(11) NOT NULL,
  PRIMARY KEY (`idx`),
  KEY `fk_idxcategory_idx` (`parent`),
  CONSTRAINT `fk_idxcategory` FOREIGN KEY (`parent`) REFERENCES `category` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (4,'생방송',NULL,1),(5,'시차방송-준비중',NULL,2),(6,'영화',NULL,3),(15,'드라마',NULL,4),(16,'최신/신규 한국',6,1),(17,'최신/신규 외국',6,2),(18,'최신/신규 애니',6,3),(19,'한국드라마',15,1),(20,'미국드라마',15,2),(21,'일본드라마',15,3),(22,'중국드라마',15,4);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `common_path`
--

DROP TABLE IF EXISTS `common_path`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `common_path` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `common_path`
--

LOCK TABLES `common_path` WRITE;
/*!40000 ALTER TABLE `common_path` DISABLE KEYS */;
INSERT INTO `common_path` VALUES (1,'upload_default_path','C:/file');
/*!40000 ALTER TABLE `common_path` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gift`
--

DROP TABLE IF EXISTS `gift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gift` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `settopgroup` int(11) DEFAULT NULL,
  `regdate` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `pin` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `usagedate` varchar(45) DEFAULT NULL,
  `madeby` varchar(45) DEFAULT NULL,
  `apply` varchar(45) DEFAULT NULL,
  `user` varchar(45) DEFAULT NULL,
  `validdatecount` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `gift_fk_idxsettopgroup_idx` (`settopgroup`),
  KEY `gift_fk_username_madeby_idx` (`madeby`),
  KEY `gift_fk_username_apply_idx` (`apply`),
  CONSTRAINT `gift_fk_idxsettopgroup` FOREIGN KEY (`settopgroup`) REFERENCES `settopgroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `gift_fk_username_apply` FOREIGN KEY (`apply`) REFERENCES `member` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `gift_fk_username_madeby` FOREIGN KEY (`madeby`) REFERENCES `member` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gift`
--

LOCK TABLES `gift` WRITE;
/*!40000 ALTER TABLE `gift` DISABLE KEYS */;
INSERT INTO `gift` VALUES (1,NULL,'2018-03-04 12:23:12','2343-2342-1231-6788-7688','234W','USED','2018-03-10 12:12:23','admin','admin','브라질 136',30);
/*!40000 ALTER TABLE `gift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `live`
--

DROP TABLE IF EXISTS `live`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `live` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `channel` int(11) DEFAULT NULL,
  `moddate` varchar(45) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `live`
--

LOCK TABLES `live` WRITE;
/*!40000 ALTER TABLE `live` DISABLE KEYS */;
INSERT INTO `live` VALUES (1,'라이브','SBS',1,'2018-01-17 01:12:34','timeshift/_definst_/mp4:channela_06h',0);
/*!40000 ALTER TABLE `live` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(150) NOT NULL,
  `nickname` varchar(45) NOT NULL,
  `groupname` int(11) DEFAULT NULL,
  `moddate` varchar(45) DEFAULT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`idx`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `member_fk_settopgroup_idx` (`groupname`),
  CONSTRAINT `member_fk_settopgroup` FOREIGN KEY (`groupname`) REFERENCES `settopgroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `member`
--

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;
INSERT INTO `member` VALUES (1,'admin','$2a$10$R7ScxMWN4lf9kpN8DRasQuHzdE8l1meNYipY6lE4fjMMKA6qt/Q3.','관리자',NULL,'2018-03-27 03:27:30','ADMIN'),(2,'alba','$2a$10$6EzruKjwptMZOygHeeJNI.Fq0gvgw7EdN6FZWFsOTWeqJdtF0GFMi','알바',1,'2018-03-27 03:27:30','USER');
/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `regdate` varchar(45) DEFAULT NULL,
  `title` text,
  PRIMARY KEY (`idx`),
  KEY `notice_fk_username_idx` (`username`),
  CONSTRAINT `notice_fk_username` FOREIGN KEY (`username`) REFERENCES `member` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
INSERT INTO `notice` VALUES (1,'admin','2018-01-01 12:12:12','공지사항 입니다.');
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice_settopgroup`
--

DROP TABLE IF EXISTS `notice_settopgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notice_settopgroup` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `idxnotice` int(11) DEFAULT NULL,
  `idxsettopgroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `fk_idxnotice_idx` (`idxnotice`),
  KEY `fk_idxsettopgroup_idx` (`idxsettopgroup`),
  CONSTRAINT `fk_idxnotice` FOREIGN KEY (`idxnotice`) REFERENCES `notice` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_idxsettopgroup` FOREIGN KEY (`idxsettopgroup`) REFERENCES `settopgroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice_settopgroup`
--

LOCK TABLES `notice_settopgroup` WRITE;
/*!40000 ALTER TABLE `notice_settopgroup` DISABLE KEYS */;
INSERT INTO `notice_settopgroup` VALUES (1,1,1),(2,1,2),(3,1,3);
/*!40000 ALTER TABLE `notice_settopgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `server`
--

DROP TABLE IF EXISTS `server`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `server` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `servergroup` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `url` varchar(45) DEFAULT NULL,
  `port` varchar(45) DEFAULT NULL,
  `isusage` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `server_fk_idxservergroup_idx` (`servergroup`),
  CONSTRAINT `server_fk_idxservergroup` FOREIGN KEY (`servergroup`) REFERENCES `servergroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `server`
--

LOCK TABLES `server` WRITE;
/*!40000 ALTER TABLE `server` DISABLE KEYS */;
INSERT INTO `server` VALUES (1,4,'한국_VOD','VOD','211.63.145.129','211.63.145.129',':1935','1'),(2,4,'한국_VOD','VOD','211.63.145.129','211.63.145.129',':1935','1'),(3,4,'한국_VODs','한국_VODs','211.63.145.129','211.63.145.129',':1935','1');
/*!40000 ALTER TABLE `server` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servergroup`
--

DROP TABLE IF EXISTS `servergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servergroup` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servergroup`
--

LOCK TABLES `servergroup` WRITE;
/*!40000 ALTER TABLE `servergroup` DISABLE KEYS */;
INSERT INTO `servergroup` VALUES (4,'한국'),(5,'미국'),(6,'브라질'),(7,'아르헨티나');
/*!40000 ALTER TABLE `servergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settop`
--

DROP TABLE IF EXISTS `settop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settop` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` int(11) DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `regdate` varchar(45) DEFAULT NULL,
  `expdate` varchar(45) DEFAULT NULL,
  `joindate` varchar(45) DEFAULT NULL,
  `isactive` varchar(1) DEFAULT NULL,
  `macaddr` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `usernumber` varchar(45) DEFAULT NULL,
  `useraddr` varchar(100) DEFAULT NULL,
  `memo` text,
  PRIMARY KEY (`idx`),
  KEY `settop_fk_settopgroup_idx` (`groupname`),
  CONSTRAINT `settop_fk_settopgroup` FOREIGN KEY (`groupname`) REFERENCES `settopgroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settop`
--

LOCK TABLES `settop` WRITE;
/*!40000 ALTER TABLE `settop` DISABLE KEYS */;
INSERT INTO `settop` VALUES (1,1,'한글테스트^^ㅗ','2018-03-21','2018-03-22','0000-00-00','1','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(2,1,'no id','2018-03-21','2018-03-22','0000-00-00','1','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(3,1,'no id','2018-03-21','2018-03-22','0000-00-00','1','C4:sEF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(4,1,'no id','2018-03-21','2018-03-22','0000-00-00','1','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(6,1,'no id','2018-03-21','2018-03-22','0000-00-00','1','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(7,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(8,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(10,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(11,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(12,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(13,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(14,1,'no id','2018-03-21','2018-03-22','0000-00-00','0','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(15,1,'no id','2018-03-21','2018-03-22','0000-00-00','1','C4:EF:23:AB:00:45','kim','010-1234-5678','addr','mm'),(16,1,'한글테스트!!','2018-03-21','2018-03-21','2018-03-21','1','C4:EF:23:AB:00:45','조우현','010-1234-5678','경기대긱사','^^우현이자러감'),(17,1,'test1','tes','tes','tes','1','tes','test','test','tes','tes'),(18,1,'2번째테스트 ','ㅇㅀ','ㅎㄹ','ㄹㅇㅎ','0','ㄹㄴㅇ','가입여부 off','ㅇㄹㄴ','ㅇㄹㄴ','ㅀㅇ'),(19,1,'3번째테스트','sdf','dsf','dfs','0','sdf','off','sdf','sfda','sdf'),(20,1,'4','dsf','dsfsdf','sdf','1','sdfsdf','on','sadf','sdf','sdf'),(22,1,'t','t','t','t','1','t','t','t','t','t');
/*!40000 ALTER TABLE `settop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settopgroup`
--

DROP TABLE IF EXISTS `settopgroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settopgroup` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `timediff` int(1) DEFAULT NULL,
  `istemp` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settopgroup`
--

LOCK TABLES `settopgroup` WRITE;
/*!40000 ALTER TABLE `settopgroup` DISABLE KEYS */;
INSERT INTO `settopgroup` VALUES (1,'한국',1,'0'),(2,'미국',9,'0'),(3,'임시폴더',0,'1');
/*!40000 ALTER TABLE `settopgroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settopgroup_servergroup`
--

DROP TABLE IF EXISTS `settopgroup_servergroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settopgroup_servergroup` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `settopgroup` int(11) DEFAULT NULL,
  `servergroup` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `fk_servergroup_idx` (`servergroup`),
  KEY `fk_settopgroup_idx` (`settopgroup`),
  CONSTRAINT `fk_servergroup` FOREIGN KEY (`servergroup`) REFERENCES `servergroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_settopgroup` FOREIGN KEY (`settopgroup`) REFERENCES `settopgroup` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settopgroup_servergroup`
--

LOCK TABLES `settopgroup_servergroup` WRITE;
/*!40000 ALTER TABLE `settopgroup_servergroup` DISABLE KEYS */;
INSERT INTO `settopgroup_servergroup` VALUES (1,1,4),(2,2,5);
/*!40000 ALTER TABLE `settopgroup_servergroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `testvarchar` varchar(45) DEFAULT NULL,
  `testint` int(1) DEFAULT NULL,
  `testtext` text,
  PRIMARY KEY (`idx`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (3,'test',1,'tes'),(4,'test',2,'test'),(5,'zzzzzz',1,'zzzzzz'),(6,'ㅅㄷㄴㅅ',1,'ㅅㄷㄴㅅ'),(7,'ets',1,'set');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vod`
--

DROP TABLE IF EXISTS `vod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vod` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `moddate` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `shortdesc` varchar(300) DEFAULT NULL,
  `desc` text,
  `imgpath` varchar(45) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `vod_fk_idxcategory_idx` (`category`),
  CONSTRAINT `vod_fk_idxcategory` FOREIGN KEY (`category`) REFERENCES `category` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vod`
--

LOCK TABLES `vod` WRITE;
/*!40000 ALTER TABLE `vod` DISABLE KEYS */;
INSERT INTO `vod` VALUES (1,'VOD',19,'도깨비','2018-03-27 00:12:34','y','공유 출연 도깨비','불멸의 삶을 끝내기 위해 인간 신부가 필요한 도깨비, 그와 기묘한 동거를 시작한 기억상실증 저승사자. 그런 그들 앞에 \'도깨비 신부\'라 주장하는 \'죽었어야 할 운명\'의 소녀가 나타나며 벌어지는 신비로운 낭만 설화','test.png',NULL);
/*!40000 ALTER TABLE `vod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vodfile`
--

DROP TABLE IF EXISTS `vodfile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vodfile` (
  `idx` int(11) NOT NULL AUTO_INCREMENT,
  `idxvod` int(11) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  `path` varchar(100) DEFAULT NULL,
  `moddate` varchar(45) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`idx`),
  KEY `vodfile_fk_vod_idx` (`idxvod`),
  CONSTRAINT `vodfile_fk_vod` FOREIGN KEY (`idxvod`) REFERENCES `vod` (`idx`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vodfile`
--

LOCK TABLES `vodfile` WRITE;
/*!40000 ALTER TABLE `vodfile` DISABLE KEYS */;
INSERT INTO `vodfile` VALUES (1,1,'도깨비.E01.170102','dokkebi_E01_170102.mp4','2018-01-02 12:12:12',1),(2,1,'도깨비.E02.170102','dokkebi_E02_170102.mp4','2018-01-02 12:12:12',2),(3,1,'도깨비.E03.170102','dokkebi_E03_170102.mp4','2018-01-02 12:12:12',3),(4,1,'도깨비.E04.170102','dokkebi_E04_170102.mp4','2018-01-02 12:12:12',4),(5,1,'도깨비.E05.170102','dokkebi_E05_170102.mp4','2018-01-02 12:12:12',5),(6,1,'도깨비.E06.170102','dokkebi_E06_170102.mp4','2018-01-02 12:12:12',6);
/*!40000 ALTER TABLE `vodfile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-05  2:29:34
