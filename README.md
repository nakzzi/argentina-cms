---

## TIP

### dependency 추가 후 적용이 안될 때
> 이클립스 종료 → repository 삭제 → maven - update project 
> → maven install → 프로젝트 clean

### git conflict
> push → non-fast-forward → pull → conflict !! 
> → syncronize workspase → 충돌 확인 → 충돌 overwrite → merge 
> → 충돌파일 push → pull → 충돌없는거 확인 → 전체 push → 깰끔


---

### settop이 요청 할 수 있는 정보  
#### 공통
- {CMS서버}/{테이블이름}/getlist.do  
	예)  
	```
	/category/getlist.do  
	```  
	```
	/live/getlist.do?tail=limit 5  
	```  
	```
	/live/getlist.do?tail=order by idx desc  
	```  
	tail은 sql문 뒤에 추가적으로 요청할 작업을 넣어줌
	(tail값은 sql injection filter에 걸러지므로 insert,update,where 이런건 못들어감.
	만약 필요하면 컨트롤러 수정.)  
	>
- {CMS서버}/{테이블이름}/getbean.do?idx={인덱스}
	예)  
	```
	/category/getbean.do?idx=32
	```   
	>
- 요청 가능한 테이블  
	1. advertise
	2. category
	3. gift
	4. live
	5. notice
	6. serverinfo
	7. servergroup
	8. settop
	9. settopgroup
	10. vod
	11. vodfile  
	  
#### category
- category/getsortedlist.do
- category/getsortedjson.do
- category/getchildren.do